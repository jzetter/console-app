# Sample Console Application
This is a skeleton version of the Symfony Console application.

## Running the console
The console can be started with by running the PHP console file.
```
$ php console
```

## PSR-2
The application is PSR-2 compliant and comes with an included Code sniffer
```
php vendor/bin/phpcs
```